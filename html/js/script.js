/**
 *	PROJECT			:		CAMWP DEV
 *	DEVELOPER		:		CAMWP DEV Team
 *	DEVELOPER URI	:		https://camwpdev.com
 *	DATE			:		20-August-2016
 */
 
$( function() {

	var AMATOP10 = {
		
		init: function() {
			
			this.featureSlider();
            this.share();
            this.stickySocial();
			
		},
		
		featureSlider: function() {
            
            if ( $( '.feature-slider' ).length ) {

                $( '.feature-slider' ).slick( {
                    infinite: true,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    autoplay: false,
                    arrows: false,
                    easing: 'ease-in-out',
                    responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 2,
                            infinite: true
                        }

                    }, {

                        breakpoint: 640,
                        settings: {
                            slidesToShow: 1,
                            dots: false
                        }

                    }]
                });

                $( '.slick-control .next a' ).on( "click", function ( e ) {
                    e.preventDefault();

                    $( '.feature-slider' ).slick( 'slickNext' );
                });
                $( '.slick-control .prev a' ).on( 'click', function ( e ) {
                    e.preventDefault();

                    $( '.feature-slider' ).slick( 'slickPrev' );
                });
            }
			
		},

        share: function() {

            if ( $( '.share' ).length ) {

                var data = $( '.share' ).attr( 'data' );

                $(".share").jsSocials({

                    showLabel: false,
                    showCount: false,
                    shareIn: "popup",
                    shares: data.split( ',' )

                });

            }

            $( '.social-hide-btn' ).on( 'click', function( e ) {

                e.preventDefault();

                $( this ).parent().toggleClass( 'hide-social' );

            });

        },

        stickySocial: function() {

            if ( $( '.sticky-social' ).length ) {
                
                if ( $( window ).width() < 768 ) {
                    
                    $( '.social-share.after-content' ).show();
                    $( '.social-share.sticky-social' ).hide();
                
                } else {
                    
                    $( '.social-share.after-content' ).hide();
                    
                }

                $( window ).scroll( function() {
                        
                    if ( $( window ).scrollTop() > 400 && $( window ).width() >= 768 ) {

                        $( '.sticky-social' ).fadeIn();

                    } else {
                        
                        $( '.sticky-social' ).fadeOut();

                    }
                    
                });
    
            }

        }
		
	}
	
	$( document ).ready( function() {
		
		AMATOP10.init();
		
	});

    $( window ).resize( function() {

        AMATOP10.stickySocial();

    });
	
});