<?php
/**
 * Plugin Name: Azon Sereyboth
 * Plugin URI: http://sereyboth.com
 * Description: For personal AZon Shortcode
 * Version: 1.0
 * Author: Sereyboth Yorn
 * Author URI: http://sereyboth.com
 * License: A "Slug" license name e.g. GPL2
 */
 //usage sample: [Azonasinid asinid="B00BFFAWHK"]
 
// Add Shortcode
function azonsc( $atts ) {
	// Attributes
	extract( shortcode_atts(
		array(
			'asinid' => '',
		), $atts )
	);
	// Code

return '<div class="amz-buttons">
	<a href="http://www.amazon.com/dp/'.$asinid.'?tag=amatop-20" rel="nofollow" target="_blank" class="amz-btn-txt">	<i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp;&nbsp;Get it now on Amazon.com </a></div>
';
}

add_shortcode( 'Azonasinid', 'azonsc' );

// add css
add_action( 'wp_enqueue_scripts', 'register_plugin_styles' );
function register_plugin_styles() {
	wp_register_style( 'AzonSereyboth', plugins_url( 'AzonSereyboth/zonstyle.css' ) );
	wp_enqueue_style( 'AzonSereyboth' );
}
 
 ?>