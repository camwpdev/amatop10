<form role="search" method="get" class="search-form form-inline" action="<?php echo home_url( '/' ); ?>">
	<div class="form-container">
	    <label>
	        <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
	        <input type="search" class="search-field form-control" placeholder="Type here..." value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" autocomplete="off" />
	    </label>
	    <button type="submit" class="search-submit btn btn-primary"><?php echo esc_attr_x( 'Search', 'submit button' ) ?></button>
    </div>
</form>