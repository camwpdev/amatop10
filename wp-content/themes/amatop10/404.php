<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package amatop10
 */

get_header(); ?>

	<main id="main">
		<div class="page-content">
			<div class="container">
				<div class="row">
					<div class="content-area content-wrapper text-center col-md-12">
						<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'amatop10' ); ?></h1>
					
						<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'amatop10' ); ?></p>
						
						<?php get_search_form(); ?>
					</div>
				</div>
			</div>
		</div>
	</main>

<?php
get_footer();
