/**
 *	PROJECT			:		CAMWP DEV
 *	DEVELOPER		:		CAMWP DEV Team
 *	DEVELOPER URI	:		https://camwpdev.com
 *	DATE			:		20-August-2016
 */
 
$( function() {

	var AMATOP10 = {		
		init: function() {
			
			this.featureSlider();
            this.share();
            this.stickySocial();
            this.toggleSearchForm();
            this.toTop();
            this.stickySidebar();
			
		},
		
		featureSlider: function() {
            
            if ( $( '.feature-slider' ).length ) {

                $( '.feature-slider' ).slick( {
                    infinite: true,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    autoplay: false,
                    arrows: false,
                    easing: 'ease-in-out',
                    responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 2,
                            infinite: true
                        }

                    }, {

                        breakpoint: 640,
                        settings: {
                            slidesToShow: 1,
                            dots: false
                        }

                    }]
                });
                
                $( '.feature-slider' ).css( 'opacity', '1' );

                $( '.slick-control .next a' ).on( "click", function ( e ) {
                    e.preventDefault();

                    $( '.feature-slider' ).slick( 'slickNext' );
                });
                $( '.slick-control .prev a' ).on( 'click', function ( e ) {
                    e.preventDefault();

                    $( '.feature-slider' ).slick( 'slickPrev' );
                });
            }
			
		},

        share: function() {

            if ( $( '.share' ).length ) {

                var data = $( '.share' ).attr( 'data' );

                $(".share").jsSocials({

                    showLabel: false,
                    showCount: false,
                    shareIn: "popup",
                    shares: data.split( ',' )

                });

            }

            $( '.social-hide-btn' ).on( 'click', function( e ) {

                e.preventDefault();

                $( this ).parent().toggleClass( 'hide-social' );

            });

        },

        stickySocial: function() {

            if ( $( '.sticky-social' ).length ) {

                $( window ).scroll( function() {
                        
                    if ( $( window ).scrollTop() > 400 ) {

                        $( '.sticky-social' ).fadeIn();

                    } else {
                        
                        $( '.sticky-social' ).fadeOut();

                    }
                    
                });
    
            }
            
            if ( $( '.social-share.sticky-social' ).length &&  $( '.social-share.after-content' ).length ) {
            	
                if ( $( window ).width() < 768 ) {
                    
                    $( '.social-share.sticky-social' ).addClass( 'hidden' );
                
                } else {
                    
                    $( '.social-share.sticky-social' ).removeClass( 'hidden' );
                    
                }
            	
            } else {
            	
            	if ( $( window ).width() < 768 ) {
            		
            		$( '.social-share.left-content' ).removeClass( 'sticky-social' );
            		$( '.social-share .social-hide-btn' ).hide();
            		
            	} else {
            		
            		$( '.social-share.left-content' ).addClass( 'sticky-social' );
            		$( '.social-share .social-hide-btn' ).show();
            		
            	}
            	
            }

        },
        
        toggleSearchForm: function() {
        	
			$( '.toggle-search' ).on( 'click', function( e ) {
				
				e.preventDefault();
				
				$( '.navbar .search-form' ).fadeToggle();
				
				$( '.navbar .search-form .search-field' ).focus();
				
				if ( $( this ).find( '.fa' ).hasClass( 'fa-search' ) ) {
					
					$( this ).find( '.fa' ).removeClass( 'fa-search' ).addClass( 'fa-times' );
					
				} else {
					
					$( this ).find( '.fa' ).removeClass( 'fa-times' ).addClass( 'fa-search' );
					
				}
				
			});
        	
        },
        
        toTop: function() {
        	
            var whereToShow = 400;
            var delay       = 500;

            AMATOP10.checkScroll( whereToShow, delay );

            $( window ).scroll( function() {

            	AMATOP10.checkScroll(whereToShow, delay);

            });

            $( '.to-top' ).click( function( e ) {

                $( 'body, html' ).animate( { scrollTop: 0 }, delay );

                e.preventDefault();

            });
        	
        },
        
        checkScroll: function(whereToShow, delay) {
        	
            if ( $( window ).scrollTop() > whereToShow ) {

                $( '.to-top' ).fadeIn( delay );

            } else {

                $( '.to-top' ).fadeOut( delay );

            }
            
        },
        
        stickySidebar: function() {
        	
        	if ( $( window ).width() > 768 ) { 
        	
	        	if ( $( '.sticky-sidebar' ).length ) {
	        		
	        		var navbarH = $( '.navbar' ).outerHeight();
	        		var sidebarH = $( '.sticky-sidebar' ).outerHeight();
	        		
	        		var footerOffsetTop = $( '#footer' ).offset().top;
	        		
	        		if ( $( 'body.home' ).length ) {
	        			
	        			var stickyH = navbarH + sidebarH;
	        			
	        		} else {
	        			
	        			var page_header = $( '.page-header' ).outerHeight();
	        			
	        			var stickyH = navbarH + sidebarH + page_header;
	        			
	        		}
	        		
	        		$( window ).scroll( function() {
	        		
		        		if ( $( window ).scrollTop() > stickyH ) {
		        			
		        			$( '.sticky-sidebar' ).css( {
		        				
		        				'position': 'absolute',
		        				'top': $( window ).scrollTop() - stickyH,
		        				'right': 0
		        				
		        			});
		        			
		        		} else {
		        		
		        			$( '.sticky-sidebar' ).css( {
		        				
		        				'position': 'static',
		        				'top': 0,
		        				'right': 0
		        				
		        			});
		        			
		        		}
		        		
	        		});
	        		
	        	}
        	
        	}
        	
        }
		
	}
	
	$( document ).ready( function() {
		
		AMATOP10.init();
		
	});

    $( window ).resize( function() {

        AMATOP10.stickySocial();

    });
	
});