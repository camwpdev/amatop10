<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package amatop10
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}

if ( function_exists( 'ot_get_option' ) ) {
	$sticky_sidebar = ot_get_option( 'sticky_sidebar', false );
}
?>

<aside id="secondary" class="widget-area hidden-xs col-md-4 <?php echo $sticky_sidebar == 'on' ? 'sticky-sidebar' : ''; ?>" role="complementary">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside><!-- #secondary -->
