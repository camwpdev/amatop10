<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package amatop10
 */

get_header(); ?>

	<main id="main">
		<header class="page-header">
			<div class="container">
				<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>				
				<?php get_template_part( 'template-parts/content', 'breadcrumb' ); ?>
			</div>
		</header>
		<div class="page-content">
			<div class="container">
				<div class="row">
					<div class="content-area clearfix">
						<div class="col-md-12">
							<?php
								while ( have_posts() ) : the_post();
					
									get_template_part( 'template-parts/content', 'page' );
					
									// If comments are open or we have at least one comment, load up the comment template.
									
									if ( ( comments_open() || get_comments_number() ) && is_page() == false ) :
										comments_template();
									endif;
					
								endwhile;
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

<?php
get_footer();
