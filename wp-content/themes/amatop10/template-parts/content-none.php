<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package amatop10
 */

?>

<section class="no-results not-found">
	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'amatop10' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>
		
			<header class="page-header">
				<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'amatop10' ); ?></h1>
			</header>

			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'amatop10' ); ?></p>
			<?php
				get_search_form();

		else : ?>
		
			<main id="main">
				<div class="page-content">
					<div class="container">
						<div class="row">
							<div class="content-area content-wrapper text-center col-md-12">
								<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'amatop10' ); ?></h1>

								<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'amatop10' ); ?></p>
								
								<?php get_search_form(); ?>
							</div>
						</div>
					</div>
				</div>
			</main>
							

		<?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
