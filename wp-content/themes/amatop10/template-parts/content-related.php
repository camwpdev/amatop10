<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package amatop10
 */

?>

<?php
	$cat = get_the_category( $post->ID ); // pull article from the same category
		
	if ( function_exists( 'get_option_tree' ) ) {
		$number_related = get_option_tree( 'number-related' );
	}
		

	if ( count( $cat > 0 ) ) :
		$args = array(
                'post_type' => 'post',
                'category_name' => $cat[0]->slug,
                'post__not_in' => array( $post->ID ),
                'posts_per_page' => $number_related
		);

		$loop = new WP_Query( $args );

		if ( $loop->have_posts() ) :

			while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    
				<div class="col-md-4">
					<div class="thumbnail">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'amatop10-list-thumbnail' ); ?></a>

						<div class="caption">
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						</div>
					</div>
				</div>

			<?php endwhile;
		endif;

		wp_reset_query();
	endif;
?>
