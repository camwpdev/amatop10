<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package amatop10
 */

?>

<?php 

	if ( function_exists( 'yoast_breadcrumb' ) ) {
	
		if ( function_exists( 'get_option_tree' ) ) {
			
			if ( get_option_tree( 'breadcrumb' ) == 'on' ) {
				
				if ( get_option_tree( 'yoast_breadcrumb' ) == 'yoast_breadcrumb_yes' ) {
				
					yoast_breadcrumb( '<nav id="breadcrumb">', '</nav>' );
					
				}				
			}
			
		}
		
	}

?>
