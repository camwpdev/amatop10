<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package amatop10
 */

global $count;

?>

<?php if ( $count == 1 ) : ?>

<div class="col-md-8">

<?php endif; ?>

<?php if ( $count == 2 ) : ?>

<div class="col-md-4">

<?php endif; ?>

	<div class="feature-thumbnail feature-thumbnail-<?php echo $count; ?>">
		<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail( 'amatop10-featured-thumbnail' ); ?>
		</a>
		<div class="caption">
			<a href="/category/<?php echo get_the_category()[0]->slug; ?>" class="category"><?php echo get_the_category()[0]->name; ?></a>
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		</div>
	</div>

<?php if ( $count == 1 || $count == 3 ) : ?>	
	
</div>

<?php endif; ?>
