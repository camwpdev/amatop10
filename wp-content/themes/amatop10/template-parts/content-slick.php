<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package amatop10
 */

?>

<div class="feature-slider-item">
	<div class="thumbnail">
		<a href="<?php the_permalink(); ?>">
	  		<?php the_post_thumbnail( 'amatop10-featured-thumbnail' ); ?>
	  	</a>
	  	<div class="caption">
	  		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	  	</div>
	</div>
</div>
