<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package amatop10
 */

?>

<div class="blog-list-thumbnail">
	<div class="thumbnail">
		<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail( 'amatop10-list-thumbnail' ); ?>
		</a>
		<div class="caption">
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<ul class="meta-post">
				<?php amatop10_posted_on(); ?>
			</ul>
			<p><?php echo excerpt( 60 ); ?></p>
		</div>
	</div>
</div>
