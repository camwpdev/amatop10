<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package amatop10
 */

global $main_category;
global $column_class;

?>

<div class="<?php echo $column_class; ?>">
	<h2 class="section-heading"><?php echo $main_category->name; ?></h2>
	
	<?php
	
		$count = 1;
	
		$args = array(
				'posts_per_page' => 4,
				'orderby' => 'post_date',
				'order' => 'DESC',
				'post_type' => 'post',
				'post_status' => 'publish',
				'category_name' => $main_category->slug
		);
		
		$posts = new WP_Query( $args );
		
		if ( $posts->have_posts() ) :
		
			while ( $posts->have_posts() ) : $posts->the_post(); ?>
			
				<?php if ( $count == 1 ) : ?>
		
				<div class="row">
					<div class="col-md-12">
						<div class="blog-list-thumbnail">
							<div class="thumbnail">
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail( 'amatop10-main-thumbnail' ); ?>
								</a>
								<div class="caption">
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<p><?php echo excerpt( 20 ); ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<?php else : ?>
				
				<div class="row">
					<div class="col-md-12">
				
						<div class="list-group-item">
							<div class="thumbnail clearfix">
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail( 'amatop10-default-thumbnail' ); ?>
								</a>
								<div class="caption">
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<p><?php echo excerpt( 6 ); ?></p>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				
				<?php endif; ?>
		
			<?php $count++; endwhile;
		
		endif; wp_reset_query();
		
	?>				
</div>
