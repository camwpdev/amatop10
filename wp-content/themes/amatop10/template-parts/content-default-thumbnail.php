<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package amatop10
 */

global $column_class;

?>

<div class="<?php echo $column_class; ?>">
	<div class="blog-list-thumbnail">
		<div class="thumbnail no-border">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'amatop10-main-thumbnail' ); ?>
			</a>
			<div class="caption">
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<p><?php echo excerpt( 8 ); ?></p>
			</div>
		</div>
	</div>			
</div>
