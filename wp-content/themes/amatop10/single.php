<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package amatop10
 */

get_header(); 

$post_sidebar = get_field( 'post_sidebar' );

?>

<main id="main" class="<?php echo $post_sidebar == 'Enable' ? '' : 'without-sidebar'; ?>">
	<header class="page-header">
		<div class="container">
			<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>				
			<?php get_template_part( 'template-parts/content', 'breadcrumb' ); ?>
		</div>
	</header>
	<div class="page-content">
		<div class="container">
			<div class="row">
				<div class="content-area clearfix">
					<div class="<?php echo $post_sidebar == 'Enable' ? 'col-md-8' : 'col-md-12'; ?>">
					
						<?php 
							if ( function_exists( 'ot_get_option' ) ) {
								$social_button_enable = ot_get_option( 'social-button-enable' );
								$social_sharing_pos = ot_get_option( 'social-sharing-pos' )[0];
								$ad_after_post_title = ot_get_option( 'ad-after-post-title' );
									
								if ( $social_sharing_pos == 'top' && $social_button_enable == 'on' ) :
									
									amatop10_social_shares( 'before-content', false );
								
								endif;
								
								echo $ad_after_post_title;
							}
						?>
					
						<article class="page-content">
							<?php the_content(); ?>
						</article>
						
						<?php 
							if ( function_exists( 'ot_get_option' ) ) {
								$social_button_enable = ot_get_option( 'social-button-enable' );
								$social_sharing_pos = ot_get_option( 'social-sharing-pos' )[1];
								$ad_below_post = ot_get_option( 'ad-below-post' );
									
								if ( $social_sharing_pos == 'bottom' && $social_button_enable == 'on' ) :
									
									amatop10_social_shares( 'after-content', false );
								
								endif;
								
								echo $ad_below_post;
							}
						?>
						
						<?php 
							if ( function_exists( 'ot_get_option' ) ) {
								$social_button_enable = ot_get_option( 'social-button-enable' );
								$social_sharing_pos = ot_get_option( 'social-sharing-pos' )[2];
								
								if ( $social_sharing_pos == 'float' && $social_button_enable == 'on' ) :
									
									amatop10_social_shares( 'sticky-social left-content', true );
								
								endif;
							}
						?>
						
						<?php 
							if ( function_exists( 'get_option_tree' ) ) {
								$post_related = get_option_tree( 'post-related' );
							}
							
							if ( $post_related == 'on' ) :
						?>
	
						<section class="related-post">
							<div class="row clearfix">
								<div class="col-md-12">
									<h2 class="section-heading">Related articles</h2>
								</div>
							</div>
	
							<div class="row clearfix">
								<?php get_template_part( 'template-parts/content', 'related' ); ?>
							</div>
						</section>
						
						<?php endif; ?>
						
						<?php 
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>
	
					</div>
					
					<?php if ( $post_sidebar == 'Enable' ) : get_sidebar(); endif; ?>
				</div>
			</div>
		</div>
	</div>
	
</main>	

<?php
get_footer();
