<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package amatop10
 */

get_header(); ?>

<div class="container clearfix">
	<div class="row">
		<div class="content-area clearfix">
			<div class="col-md-8">
				<section id="feature">
					<div class="row">
						<div class="col-md-12">
							<h2 class="section-heading">Featured</h2>
						</div>
					</div>
					<div class="row">
						<?php 
							$args = array(
								'posts_per_page' => 3,
								'post__in' => get_option( 'sticky_posts' ),
								'ignore_sticky_posts' => 1,
								'orderby' => 'post_date',
								'order' => 'DESC',
								'post_type' => 'post',
								'post_status' => 'publish'
							);
							
							$count = 1;
							
							$sticky_posts = new WP_Query( $args );
							
							if ( $sticky_posts->have_posts() ) :
							
								while( $sticky_posts->have_posts()) : $sticky_posts->the_post();
							
									get_template_part('template-parts/content', 'featured');
									
									$count++;
							
								endwhile;
							
							else :
							
								get_template_part('template-parts/content', 'none');
							
							endif; wp_reset_query();
						?>
					</div>
				</section>
				
				<section id="main-category">
					<div class="row display-flex">
						<?php 
							$page = get_page_by_title( 'Home' );
							
							$main_categories = get_field( 'main_categories', $page->ID );							
							
							$column_class = 'col-md-6';
							
							foreach ( $main_categories as $main_category ) :
							
								get_template_part('template-parts/content', 'main-category');
							
							endforeach;
						?>
					</div>
				</section>
				
				<section id="latest-post">
					<div class="row">
						<div class="col-md-12">
							<h2 class="section-heading">Latest Post</h2>
						</div>
					</div>
					
					<div class="row display-flex">
						<?php					
							$args = array(
								'posts_per_page' => 6,
								'post__not__in' => get_option( 'sticky_posts' ),
								'ignore_sticky_posts' => 1,
								'orderby' => 'post_date',
								'order' => 'DESC',
								'post_type' => 'post',
								'post_status' => 'publish'
							);
							
							$column_class = 'col-md-4';
							
							$latest_posts = new WP_Query( $args );
							
							if ( $latest_posts->have_posts() ) :
							
								while ( $latest_posts->have_posts() ) : $latest_posts->the_post();
							
									get_template_part('template-parts/content', 'default-thumbnail');
								
								endwhile; 
								
							endif; wp_reset_query();
						?>
					</div>
				</section>
			</div>
		
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>

<?php
get_footer();
