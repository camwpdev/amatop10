<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package amatop10
 */

?>

<?php 

	if ( function_exists( 'ot_get_option' ) ) {
		$copy_right = ot_get_option( 'copyright', 'No copy right input' );
		$tracking_code = ot_get_option( 'tracking-code', false );
		$back_to_top = ot_get_option( 'back_to_top', false );
	}

?>

<footer id="footer">
	<div class="container">
		
		<div class="copy-right col-md-6">
			<span><?php echo $copy_right; ?></span> <span>Powered by <a href="http://camwpdev.com" target="_blank">CAMWP DEV</a></span>
		</div>
		<div class="powered-by col-md-6">
			<?php echo amatop10_menu( 'footer' ); ?>
		</div>
		
		<?php if ( $back_to_top == 'on' ) : ?>
		
		<a href="#" class="to-top" title="Back To Top">
			<i class="fa fa-angle-up"></i>
		</a>
		
		<?php endif; ?>
		
	</div>
</footer>

<?php wp_footer(); ?>

<script type="text/javascript">
	<?php echo $tracking_code; //If you have tracking code (Google Analytic or other ), Pastes Your Code here which be inserted before the closing body tag of your theme. ?>
</script>

</body>
</html>
