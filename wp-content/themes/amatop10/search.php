<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package amatop10
 */

get_header(); ?>

	<main id="main">
	
		<?php if ( have_posts() ) : ?>
	
		<header class="page-header">
			<div class="container">
				<?php the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>				
				<?php get_template_part( 'template-parts/content', 'breadcrumb' ); ?>
			</div>
		</header>
		
		<div class="page-content">
			<div class="container">
				<div class="row">
					<div class="content-area clearfix">
						<div class="col-md-8">
	
							<?php
								while ( have_posts() ) : the_post();
					
									get_template_part( 'template-parts/content', 'list' );
					
								endwhile;
					
								amatop10_pagination();
							?>
		
						</div>
						
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>
		</div>
		
		<?php else : get_template_part( 'template-parts/content', 'none' ); endif; ?>
		
	</main>	

<?php
get_footer();
