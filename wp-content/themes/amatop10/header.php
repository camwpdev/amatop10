<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package amatop10
 */

if ( function_exists( 'get_option_tree' ) ) {

	$logo = ot_get_option( 'logo', get_template_directory_uri() . '/images/logo.png' );
	$header_code = ot_get_option( 'header-code', false );
	$custom_style = ot_get_option( 'custom_style', false );

}

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="<?php echo function_exists('ot_get_option') ? ot_get_option('favicon') : esc_url(get_template_directory_uri()) . '/images/favicon.png'; ?>" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	
	<style type="text/css"><?php echo $custom_style; ?></style>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php echo $header_code; // If you have any code you want to appear between and , Pastes it here. For example: Google Web Master Cool Code or Pinterest Verify Code. ?>


<nav class="navbar navbar-default navbar-custom">
	<div class="container">		
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo $logo; ?>" alt="<?php bloginfo( 'name' ); ?>" width="132" height="79" /></a>
		</div>

		<div class="collapse navbar-collapse" id="main-menu">
        	<ul class="nav navbar-nav navbar-nav-left">
				<?php echo amatop10_menu( 'header-left' ) ?>
			</ul>
        
	        <ul class="nav navbar-nav navbar-nav-right">
				<?php echo amatop10_menu( 'header-right' ) ?>
			</ul>
			
			<?php get_search_form(); ?>
			
			<span class="toggle-search hidden-xs"><i class="fa fa-search"></i></span>
			<ul class="social-network hidden-xs">
				<?php echo amatop10_social_link(); ?>
			</ul>
		</div>
	</div>
</nav>
