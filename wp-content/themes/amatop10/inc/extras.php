<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package amatop10
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function amatop10_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'amatop10_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function amatop10_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'amatop10_pingback_header' );

/**
 * Menu
 */
function amatop10_menu( $menu_name ) {
	$args = array(
			'theme_location'  => $menu_name,
			'menu'            => '',
			'container'       => false,
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => 'nav navbar-nav pull-right',
			'menu_id'         => '',
			'echo'            => false,
			'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 2,
			'walker'          => new wp_bootstrap_navwalker()
	);

	$menu = wp_nav_menu( $args );
	return preg_replace( array( '#^<ul[^>]*>#', '#</ul>$#' ), '', $menu );
}

/**
 * Enqueue scripts and styles.
 */
if ( ! is_admin() ) add_action( "wp_enqueue_scripts", "my_jquery_enqueue", 11 );

function my_jquery_enqueue() {
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery-3.1.1.min.js', false, null);
	wp_enqueue_script( 'jquery' );
}

function amatop10_scripts() {
	wp_enqueue_style( 'amatop10-style', get_stylesheet_uri() );
	wp_enqueue_style( 'amatop10-font', 'https://fonts.googleapis.com/css?family=Hind+Vadodara:300,400,500,600,700' );
	

	wp_enqueue_script( 'amatop10-bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '20170213', true );
	wp_enqueue_script( 'amatop10-slick-script', get_template_directory_uri() . '/js/slick.min.js', array(), '20170213', true );
	wp_enqueue_script( 'amatop10-social-share-script', get_template_directory_uri() . '/js/jssocials.min.js', array(), '20170213', true );
	wp_enqueue_script( 'amatop10-main-script', get_template_directory_uri() . '/js/script.js', array(), '20170213', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'amatop10_scripts' );

/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package amatop10
 */
function excerpt( $limit ) {
	$excerpt = explode( ' ', get_the_excerpt(), $limit );
	if ( count( $excerpt ) >= $limit ) {
		array_pop( $excerpt );
		$excerpt = implode( " ", $excerpt ) . '...';
	} else {
		$excerpt = implode( " ", $excerpt );
	}
	$excerpt = preg_replace( '`\[[^\]]*\]`', '', $excerpt );
	
	return $excerpt;
}

function content( $limit ) {
	$content = explode( ' ', get_the_content(), $limit );
	if ( count( $content ) >= $limit ) {
		array_pop( $content );
		$content = implode( " ", $content ) . '...';
	} else {
		$content = implode( " ", $content );
	}
	$content = preg_replace( '/\[.+\]/', '', $content );
	$content = apply_filters( 'the_content', $content );
	$content = str_replace( ']]>', ']]&gt;', $content );
	
	return $content;
}

/**
 * Pagination
 */
function amatop10_pagination() {
	global $wp_query;

	$args = array(
			'base'           => str_replace( 999999999, '%#%', get_pagenum_link( 999999999 ) ),
			'format'         => '',
			'current'        => max( 1, get_query_var( 'paged' ) ),
			'total'          => $wp_query->max_num_pages,
			'prev_text'      => '&#171;',
			'next_text'      => '&#187;',
			'type'           => 'list',
			'end_size'       => 3,
			'mid_size'       => 3
	);
	echo str_replace( "<ul class='page-numbers'>", "<ul class='pagination pagination-round'>", paginate_links( $args ) );
}

/**
 * Social Share
 */
function amatop10_social_shares( $position, $sticky ) {
	$social_twitter = ot_get_option( 'social-twitter' ) == 'on' ? ',twitter' : '';
	$social_facebook = ot_get_option( 'social-facebook' ) == 'on' ? ',facebook' : '';
	$social_google_plus = ot_get_option( 'social-google-plus' ) == 'on' ? ',googleplus' : '';
	$social_linkedin = ot_get_option( 'social-linkedin' ) == 'on' ? ',linkedin' : '';
	$social_pinterest = ot_get_option( 'social-pinterest' ) == 'on' ? ',pinterest' : '';
	
	$data = $social_facebook . $social_twitter . $social_google_plus . $social_linkedin . $social_pinterest;
?>
<section class="social-share <?php echo $position; ?>">
		
	<?php if ( $sticky ) : ?>
	
		<div class="social-hide-btn">
			<span class="fa fa-mail-reply-all"></span>
		</div>
		<div class="share" data="<?php echo ltrim( $data, ',' ); ?>"></div>
	
	
	<?php else : ?>
	
	<div class="row clearfix">
		<div class="col-md-12">
			<div class="share" data="<?php echo ltrim( $data, ',' ); ?>"></div>
		</div>
	</div>
	
	<?php endif; ?>
		
</section>
<?php 
}

/**
 * Wordpress Comment
 * @param unknown $comment
 * @param unknown $args
 * @param unknown $depth
 */
function amatop10_comment( $comment, $args, $depth ) {
	if ( 'div' === $args['style'] ) {
		$tag       = 'div';
		$add_below = 'comment';
	} else {
		$tag       = 'li';
		$add_below = 'div-comment';
	}
?>
    <<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>
    
    <div class="comment-author vcard">
        <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
    </div>
    
    <div class="comment-content">
		<?php printf( __( '<strong class="fn">%s</strong>' ), get_comment_author_link() ); ?>
		
		<span class="comment-meta"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
        	<?php
        		/* translators: 1: date, 2: time */
        		printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)' ), '  ', '' );
        	?>
    	</span>
    </div>
    
    <?php if ( $comment->comment_approved == '0' ) : ?>
         <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em><br />
    <?php endif; ?>

    <?php comment_text(); ?>

    <div class="reply">
        <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => 'Reply' ) ) ); ?>
    </div>
    <?php if ( 'div' != $args['style'] ) : ?>
    </div>
    <?php endif; ?>
    <?php
}


/**
 * get social link
 */
function amatop10_social_link() {
	
	if ( function_exists( 'get_option_tree' ) ) :
	
		$social_links = ot_get_option( 'social_network', false );
		
		if ( $social_links != false ) : foreach ( $social_links as $social_link ) : if ( $social_link['href'] != '' ) : ?>
		
			<li><a target="_blank" href="<?php echo $social_link['href']; ?>" title="<?php echo $social_link['name']; ?>"><i class="fa fa-<?php echo strtolower( $social_link['name'] ); ?>"></i></a></li>
		
		<?php endif; endforeach; endif;
	
	endif;
}