<?php
/**
 * Initialize the custom Theme Options.
 */
add_action ( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 *
 * @return void
 * @since 2.3.0
 */
function custom_theme_options() {
	
	/* OptionTree is not loaded yet */
	if ( ! function_exists ( 'ot_settings_id' ) )
		return false;
	
	/**
	 * Get a copy of the saved settings array.
	 */
	$saved_settings = get_option( ot_settings_id (), array () );
	
	/**
	 * Custom settings array that will eventually be
	 * passes to the OptionTree Settings API Class.
	 */
	$custom_settings = array (
		'contextual_help' => array (
			'content' => array (
				array (
					'id' => 'option_types_help',
					'title' => __ ( 'Option Types', 'amatop10' ),
					'content' => '<p>' . __ ( 'Help content goes here!', 'amatop10' ) . '</p>' 
				) 
			),
			'sidebar' => '<p>' . __ ( 'Sidebar content goes here!', 'amatop10' ) . '</p>' 
		),
		'sections' => array (
			array (
				'id' => 'option_general',
				'title' => __ ( 'General', 'amatop10' ) 
			),
			array (
				'id' => 'option_style',
				'title' => __ ( 'Styles', 'amatop10' ) 
			),
			array (
				'id' => 'option_header',
				'title' => __ ( 'Header', 'amatop10' ) 
			),
			array (
				'id' => 'option_footer',
				'title' => __ ( 'Footer', 'amatop10' ) 
			),
			array (
				'id' => 'option_single',
				'title' => __ ( 'Single Post', 'amatop10' ) 
			),
			array (
				'id' => 'option_social_button',
				'title' => __ ( 'Social Buttons', 'amatop10' ) 
			),
			array (
				'id' => 'option_ad_mangment',
				'title' => __ ( 'Ad Managment', 'amatop10' ) 
			),
			array (
				'id' => 'option_import_export',
				'title' => __ ( 'Import/Export', 'amatop10' ) 
			) 
		),
			
		'settings' => array( 
           
     		/* General */
			array (
				'id' => 'header-code',
				'label' => __ ( 'Header Code', 'amatop10' ),
				'desc' => __ ( 'If you have any code you want to appear between and , Pastes it here. For example: Google Web Master Cool Code or Pinterest Verify Code.', 'amatop10' ),
				'type' => 'textarea-simple',
				'section' => 'option_general',
				'rows' => '3' 
			),
			array (
				'id' => 'tracking-code',
				'label' => __ ( 'Footer Code (Tracing, JavaScript Code)', 'amatop10' ),
				'desc' => __ ( 'If you have tracking code (Google Analytic or other ), Pastes Your Code here which be inserted before the closing body tag of your theme.', 'amatop10' ),
				'type' => 'textarea-simple',
				'section' => 'option_general',
				'rows' => '3' 
			),
			array (
				'id' => 'back_to_top',
				'label' => __ ( 'Back To Top Button', 'amatop10' ),
				'desc' => __ ( 'Use this option to hide or show "Back To Top" button.', 'amatop10' ),
				'std' => 'on',
				'type' => 'on-off',
				'section' => 'option_general' 
			),
			array (
				'id' => 'sticky_sidebar',
				'label' => __ ( 'Sticky Sidebar', 'amatop10' ),
				'desc' => __ ( 'Use this option to make sidebar sticky.', 'amatop10' ),
				'std' => 'on',
				'type' => 'on-off',
				'section' => 'option_general'
			),

        	/* Header */
            array (
				'id' => 'logo',
				'label' => __ ( 'Custom Logo Image', 'amatop10' ),
				'desc' => __ ( 'Upload your custom logo image here.', 'amatop10' ),
				'type' => 'upload',
				'section' => 'option_header' 
			),
			array (
				'id' => 'favicon',
				'label' => __ ( 'Favicon', 'amatop10' ),
				'desc' => __ ( 'The Favicon is used as a browser and app icon for your site. Icons must be square, and at least 512 pixels wide and tall.', 'amatop10' ),
				'type' => 'upload',
				'section' => 'option_header'
			),
				
            
        	/* Footer */
			array (
				'id' => 'copyright',
				'label' => __ ( 'Footer Copyright Text', 'amatop10' ),
				'desc' => __ ( 'Add Text you want to show in "Copyright Sction" at Bottom Left of your Website.', 'amatop10' ),
				'type' => 'textarea-simple',
				'rows' => '3',
				'section' => 'option_footer' 
			),


    		/* Single */
			array (
				'id' => 'breadcrumb',
				'label' => __ ( 'Breadcrumbs', 'amatop10' ),
				'desc' => __ ( 'Use This Option to enable or Disable breadcrumbs.', 'amatop10' ),
				'std' => 'off',
				'type' => 'on-off',
				'section' => 'option_single' 
			),
			array (
				'id' => 'yoast_breadcrumb',
				'desc' => __ ( 'Check it if you install WordPress SEO by Yoast', 'amatop10' ),
				'type' => 'checkbox',
				'section' => 'option_single',
				'choices' => array (
					array (
						'value' => 'yoast_breadcrumb_yes',
						'label' => __ ( 'Use Yoast Breadcrumb', 'option-tree-theme' ) 
					)
						 
				) 
			),
			array (
				'id' => 'post-related',
				'label' => __ ( 'Related Posts', 'amatop10' ),
				'desc' => __ ( 'Use This Option to enable or Disable related Posts below your Post.', 'amatop10' ),
				'std' => 'on',
				'type' => 'on-off',
				'section' => 'option_single' 
			),
			array (
				'id' => 'related-posts-taxonomy',
				'label' => __ ( 'Related Posts Taxonomy', 'amatop10' ),
				'desc' => __ ( 'Choose the Way Related Post Work.', 'amatop10' ),
				'std' => 'tag',
				'type' => 'radio',
				'section' => 'option_single',
				'choices' => array (
					array (
						'value' => 'category',
						'label' => 'Categories' 
					) 
				) 
			),
			array (
				'id' => 'number-related',
				'std' => '3',
				'label' => __ ( 'Number Related Posts', 'amatop10' ),
				'desc' => __ ( 'Enter the number of posts to show in the related posts section.', 'amatop10' ),
				'type' => 'text',
				'section' => 'option_single' 
			),
			array (
				'id' => 'display-related-posts',
				'label' => __ ( 'Display Related Posts', 'amatop10' ),
				'desc' => __ ( 'Related Posts type to display.' ),
				'std' => 'thumbnail',
				'type' => 'radio',
				'section' => 'option_single',
				'choices' => array (
					array (
						'value' => 'thumbnail',
						'label' => __ ( 'Thumbnail and Title', 'amatop10' ) 
					)
				) 
			),
				
        	/* Social Buttons */
        	array (
				'id' => 'social-button-enable',
				'label' => __ ( 'Social Sharing Button', 'amatop10' ),
				'desc' => __ ( 'Use This Option to Show Social Sharing Button.', 'amatop10' ),
				'std' => 'on',
				'type' => 'on-off',
				'section' => 'option_social_button' 
			),
			array (
				'id' => 'social-sharing-pos',
				'label' => __ ( 'Social Sharing Buttons Position', 'amatop10' ),
				'desc' => __ ( 'Choose position for Social Sharing Buttons.', 'amatop10' ),
				'type' => 'checkbox',
				'section' => 'option_social_button',
				'std' => 'top',
				'choices' => array (
					array (
						'value' => 'top',
						'label' => __ ( 'After Title', 'amatop10' ) 
					),
					array (
						'value' => 'bottom',
						'label' => __ ( 'After Post', 'amatop10' ) 
					),
					array (
						'value' => 'float',
						'label' => __ ( 'Float Social To Left', 'amatop10' ) 
					) 
				) 
			),
			array (
				'id' => 'social-twitter',
				'label' => __ ( 'Twitter', 'amatop10' ),
				'std' => 'on',
				'type' => 'on-off',
				'section' => 'option_social_button' 
			),
			array (
				'id' => 'social-facebook',
				'label' => __ ( 'Facebook', 'amatop10' ),
				'std' => 'on',
				'type' => 'on-off',
				'section' => 'option_social_button' 
			),
			array (
				'id' => 'social-google-plus',
				'label' => __ ( 'Google Plus', 'amatop10' ),
				'std' => 'on',
				'type' => 'on-off',
				'section' => 'option_social_button' 
			),
			array (
				'id' => 'social-linkedin',
				'label' => __ ( 'Linkedin', 'amatop10' ),
				'std' => 'off',
				'type' => 'on-off',
				'section' => 'option_social_button' 
			),
			array (
				'id' => 'social-pinterest',
				'label' => __ ( 'Pinterest', 'amatop10' ),
				'std' => 'off',
				'type' => 'on-off',
				'section' => 'option_social_button' 
			),
			array (
				'id' => 'social_network',
				'label' => 'Social Network',
				'desc' => '',
				'std' => '',
				'type' => 'social-links',
				'section' => 'option_social_button',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'min_max_step' => '',
				'class' => '',
				'condition' => '',
				'operator' => 'and' 
			),
				
        	/* Ad Managment */
			array (
				'id' => 'ad-after-post-title',
				'label' => __ ( 'Below Post Title', 'amatop10' ),
				'desc' => __ ( 'Paste Your Ads Code Here ( Adsense, Buy Sell Ads ). It will be shown Below Each Post title.', 'amatop10' ),
				'type' => 'textarea-simple',
				'section' => 'option_ad_mangment',
				'rows' => '3' 
			),
			array (
				'id' => 'ad-middle-post',
				'label' => __ ( 'Middle Post Content', 'amatop10' ),
				'desc' => __ ( 'Paste Your Ads Code Here ( Adsense, Buy Sell Ads ). It will be shown right middle of your post.', 'amatop10' ),
				'type' => 'textarea-simple',
				'section' => 'option_ad_mangment',
				'rows' => '3' 
			),
			array (
				'id' => 'ad-below-post',
				'label' => __ ( 'Below Post content', 'amatop10' ),
				'desc' => __ ( 'Paste Your Ads Code Here ( Adsense, Buy Sell Ads ). It will be shown Below Each Post.', 'amatop10' ),
				'type' => 'textarea-simple',
				'section' => 'option_ad_mangment',
				'rows' => '3' 
			),
			array (
				'id' => 'duration-ads-top',
				'label' => __ ( 'Ads and Post Age', 'amatop10' ),
				'desc' => __ ( 'Only Show Ads on Post Which is Older than number of days you choose.', 'amatop10' ),
				'type' => 'numeric-slider',
				'std' => '100',
				'section' => 'option_ad_mangment' 
			),
		
			/* Style */
			array (
				'id' => 'custom_style',
				'label' => __ ( 'Custom CSS', 'amatop10' ),
				'desc' => __ ( 'Add you CSS Custom Code here. (Only For Advance User)', 'amatop10' ),
				'type' => 'css',
				'section' => 'option_style',
				'rows' => '3' 
			),
			array (
				'id' => 'text_desscription_import_export',
				'desc' => sprintf ( __ ( '<b>Click link to <a href="%s">Import/Export</a></b>', 'amatop10' ), admin_url ( 'themes.php?page=tc-theme-backup' ) ),
				'std' => '',
				'type' => 'textblock',
				'section' => 'option_import_export' 
			)
		) 
	);
	
	/* allow settings to be filtered before saving */
	$custom_settings = apply_filters ( ot_settings_id () . '_args', $custom_settings );
	
	/* settings are not the same update the DB */
	if ($saved_settings !== $custom_settings) {
		update_option ( ot_settings_id (), $custom_settings );
	}
	
	/* Lets OptionTree know the UI Builder is being overridden */
	global $ot_has_custom_theme_options;
	$ot_has_custom_theme_options = true;
}

/*
 * --------------------------------------------------------------------
 *
 * Default Font Settings
 *
 * --------------------------------------------------------------------
 */
if (function_exists ( 'register_typography' )) {
	register_typography ( array (
			'Logo Font' => array (
				'preview_text' => 'Logo',
				'preview_color' => 'dark',
				'font_family' => 'Montserrat',
				'font_variant' => 'normal',
				'font_size' => '34px',
				'css_selectors' => '' 
			),
			'primary_navigation_font' => array (
				'preview_text' => 'Primary Navigation',
				'preview_color' => 'dark',
				'font_family' => 'Montserrat',
				'font_variant' => 'normal',
				'font_size' => '14px',
				'font_color' => '#fff',
				'additional_css' => 'text-transform: uppercase;',
				'css_selectors' => '' 
			),
			'home_title_font' => array (
				'preview_text' => 'Home Post Title',
				'preview_color' => 'light',
				'font_family' => 'Montserrat',
				'font_size' => '24px',
				'font_variant' => '700',
				'font_color' => '#333',
				'css_selectors' => '' 
			),
			'single_title_font' => array (
				'preview_text' => 'Single Post Title',
				'preview_color' => 'light',
				'font_family' => 'Montserrat',
				'font_size' => '32px',
				'font_variant' => '700',
				'font_color' => '#333',
				'css_selectors' => '' 
			),
			'content_font' => array (
				'preview_text' => 'Body Text',
				'preview_color' => 'light',
				'font_family' => 'Open Sans',
				'font_size' => '18px',
				'font_variant' => 'normal',
				'font_color' => '#333',
				'css_selectors' => '' 
			),
			'sidebar_title_font' => array (
				'preview_text' => 'Sidebar Title',
				'preview_color' => 'light',
				'font_family' => 'Montserrat',
				'font_variant' => '500',
				'font_size' => '18px',
				'font_color' => '#333',
				'css_selectors' => '',
				'additional_css' => 'text-transform: uppercase;' 
			),
			'h1_headline' => array (
				'preview_text' => 'H1 Headline',
				'preview_color' => 'light',
				'font_family' => 'Montserrat',
				'font_variant' => 'normal',
				'font_size' => '36px',
				'font_color' => '#333',
				'css_selectors' => 'h1' 
			),
			'h2_headline' => array (
				'preview_text' => 'H2 Headline',
				'preview_color' => 'light',
				'font_family' => 'Montserrat',
				'font_variant' => 'normal',
				'font_size' => '32px',
				'font_color' => '#333',
				'css_selectors' => 'h2' 
			),
			'h3_headline' => array (
				'preview_text' => 'H3 Headline',
				'preview_color' => 'light',
				'font_family' => 'Montserrat',
				'font_variant' => 'normal',
				'font_size' => '24px',
				'font_color' => '#333',
				'css_selectors' => 'h3' 
			),
			'h4_headline' => array (
				'preview_text' => 'H4 Headline',
				'preview_color' => 'light',
				'font_family' => 'Montserrat',
				'font_variant' => 'normal',
				'font_size' => '20px',
				'font_color' => '#333',
				'css_selectors' => 'h4' 
			),
			'h5_headline' => array (
				'preview_text' => 'H5 Headline',
				'preview_color' => 'light',
				'font_family' => 'Montserrat',
				'font_variant' => 'normal',
				'font_size' => '18px',
				'font_color' => '#333',
				'css_selectors' => 'h5' 
			),
			'h6_headline' => array (
				'preview_text' => 'H6 Headline',
				'preview_color' => 'light',
				'font_family' => 'Montserrat',
				'font_variant' => 'normal',
				'font_size' => '16px',
				'font_color' => '#333',
				'css_selectors' => 'h6' 
			) 
	) );
}